﻿using UnityEngine;
using System.Collections;

public class RTGameManager : MonoBehaviour 
{
	#region Fields and Properties

	public static RTGameManager SharedGameManager {get; set;}

	#endregion

	#region Lifecycle

	void Awake() 
	{
		if (RTGameManager.SharedGameManager == null)
		{
			RTGameManager.SharedGameManager = this;
		}
	}

	// Use this for initialization
	void Start() 
	{
	
	}
	
	// Update is called once per frame
	void Update() 
	{
	
	}

	#endregion
}
